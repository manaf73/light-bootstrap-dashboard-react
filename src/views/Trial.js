import React from "react";
import ChartistGraph from "react-chartist";
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import Details from './details.json';
import './Trial.css';

function Trial() {
  return (
    <>
    {Details.map(post => {
  
      return (
    
      <Container fluid>
        <style>{'body { background-color: black; }'}</style>
        <Row>
        <Col lg="4" sm="6" style={{color: 'white'}}>  
            <Card className >
              <Card.Body className="cardx" >

                <div className="globe" >
                   <i class="fas fa-globe" style={{color: 'orange'}}></i> 
                   {"\n"}{post.domain} / {"\n"}   
                  <i className="th" > 
                  <i class="fas fa-th" style={{color: 'orange'}}></i></i>
                  {"\n"}{post.system} <p></p>
                </div>
                {post.apiId}{"\n"}
                <p>{post.pic}
                <Card.Title as="h4">
                
                <i>
                { 
                post.txStatus === 'HEALTHY' ? 
                <div className="HEALTHY" style={{color: '#4dffdb'}}>
                  {post.txStatus}
                </div> :
                <div className="UNHEALTHY" style={{color: '#ff3333'}}>
                  {post.txStatus}
                </div>                 
                }
                </i>
                
                    {/* <i style={{color: '#4dffdb'}}>
                    {post.txStatus}</i> */}
                </Card.Title>
                <i class="fas fa-calendar"></i>{"\n"}Since: {post.auditDatetime}
                <p><i class="fas fa-clock"> </i>{"\n"}Response Time : {post.responseTime} ms 
                            
                </p></p>
                { 
                post.txStatus === 'HEALTHY' ? 
                <div className="icon text-right">
                      <i class="fas fa-thumbs-up" style={{color: 'blue', fontSize:'30px'}}></i>
                </div> :
                <div className="icon text-right">
                <i class="fas fa-heart-broken" style={{color: 'red', fontSize:'30px'}} ></i>
                </div>                 
                }
                
              </Card.Body>
              
            </Card>
          </Col>

          <Col lg="4" sm="6" style={{color: 'white'}}>  
            <Card className="cardx" >
              <Card.Body>

                
                <div className="globe" >
                   <i class="fas fa-globe" style={{color: 'orange'}}></i> 
                   {"\n"}{post.domain} / {"\n"}   
                  <i className="th" > 
                  <i class="fas fa-th" style={{color: 'orange'}}></i></i>
                  {"\n"}{post.system} <p></p>
                </div>
                {post.apiId}{"\n"}
                <p>{post.pic}
                <Card.Title as="h4">
                <i>
                { 
                post.txStatus === 'HEALTHY' ? 
                <div className="HEALTHY" style={{color: '#4dffdb'}}>
                  {post.txStatus}
                </div> :
                <div className="UNHEALTHY" style={{color: '#ff3333'}}>
                  {post.txStatus}
                </div>                 
                }
                </i>
                    {/* <i style={{color: '#4dffdb'}}>
                    {post.txStatus}</i> */}
                </Card.Title>
                <i class="fas fa-calendar"></i>{"\n"}Since: {post.auditDatetime}
                <p><i class="fas fa-clock"> </i>{"\n"}Response Time : {post.responseTime} ms 
                            
                </p></p>
                { 
                post.txStatus === 'HEALTHY' ? 
                <div className="icon-big text-right icon-primary">
                      <i className="nc-icon nc-favourite-28 text-primary"></i>
                </div> :
                <div className="icon-big text-right icon-warning">
                <i className="nc-icon nc-fav-remove text-warning"></i>
                </div>                 
                }
                

              </Card.Body>
              
            </Card>
          </Col>
          

          <Col lg="4" sm="6" style={{color: 'white'}}>  
            <Card className="cardx" >
              <Card.Body>

                
                <div className="globe" >
                   <i class="fas fa-globe" style={{color: 'orange'}}></i> 
                   {"\n"}{post.domain} / {"\n"}   
                  <i className="th" > 
                  <i class="fas fa-th" style={{color: 'orange'}}></i></i>
                  {"\n"}{post.system} <p></p>
                </div>
                {post.apiId}{"\n"}
                <p>{post.pic}
                <Card.Title as="h4">
                <i>
                { 
                post.txStatus === 'HEALTHY' ? 
                <div className="HEALTHY" style={{color: '#4dffdb'}}>
                  {post.txStatus}
                </div> :
                <div className="UNHEALTHY" style={{color: '#ff3333'}}>
                  {post.txStatus}
                </div>                 
                }
                </i>
                    {/* <i style={{color: '#4dffdb'}}>
                    {post.txStatus}</i> */}
                </Card.Title>
                <i class="fas fa-calendar"></i>{"\n"}Since: {post.auditDatetime}
                <p><i class="fas fa-clock"> </i>{"\n"}Response Time : {post.responseTime} ms 
                            
                </p></p>
                { 
                post.txStatus === 'HEALTHY' ? 
                <div className="icon-big text-right icon-primary">
                      <i className="nc-icon nc-favourite-28 text-primary"></i>
                </div> :
                <div className="icon-big text-right icon-warning">
                <i className="nc-icon nc-fav-remove text-warning"></i>
                </div>                 
                }
                
                  
                
              </Card.Body>
              
            </Card>
          </Col>



        </Row>

        
        
        
        
      </Container>
      
       )
      }) }
    </>
  );
}

export default Trial;
